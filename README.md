# TEM-Analyse-Pipeline

# Installation
## mistral
### Installation of cdi.jl
he following guide show you how to install the original libcdi followed by cdi.jl on mistral. To follow this guide you have to run the commands on a login node.

1. Load necessary modules
```bash
. /sw/rhel6-x64/etc/profile.mistral
module use /sw/spack-rhel6/spack/modules/linux-rhel6-haswell
module load libtool autoconf automake gcc/7.1.0
```

2. Clone the libcdi repository to your local account
```bash
git clone https://gitlab.dkrz.de/mpim-sw/libcdi.git
cd libcdi
git checkout develop
./autogen.sh
```

3. Create a folder where the final library would save. A typical place should be `/pf/m/[YOUR USER]/local`. **You must change `[YOUR USER]`!** This is used in the following guide. If you want to use a different location, please change the path.
```bash
mkdir ~/local
autoreconf -i
./configure --prefix="/pf/m/[YOUR USER]/local" --with-netcdf=/sw/rhel6-x64/netcdf/netcdf_c-4.6.1-gcc64 --with-eccodes=/sw/rhel6-x64/eccodes/eccodes-2.6.0-gcc64
make
make install
```

4. Export the newly created library to the `LD_LIBRARY_PATH` system variable. If you want to use cdi.jl in the future again, it could be a good idea to write the following lines into your `~/.bashrc` file. If you did these, do not forget to reload the config with `source ~/.bashrc`.
**You must change `[YOUR USER]`!**
``` bash
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/sw/rhel6-x64/eccodes/eccodes-2.6.0-gcc64/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/pf/m/[YOUR USER]/local/lib
```

### Installation of TEM-Analyse-Pipeline
``` bash
cd ~
module add gcc/7.1.0 julia/1.5.2-gcc-9.1.0
git clone https://gitlab.dkrz.de/julia/tem-analyse-pipeline.git
cd tem-analyse-pipeline
julia --project=@. -e "import Pkg; Pkg.instantiate()"
```

# Configure
Please change the parameter at the "tem-calc.jl" file to change the input and output parameters of the TEM-Analyse-Pipeline. A section of the config is shown below:
``` julia
const config = Config()
config["experiment"] = "laach002_l95_t63"

config["firstDate"] = Date(2017)
config["lastDate"]  = Date(2017, 12)

config["base_dir"] = "/scratch/local1/tem-analyse/data" |> normpath

# original model output can be found here:
config["data_dir"] = joinpath(config["base_dir"], config["experiment"]) |> normpath

# 6 hourly output directory
config["work_dir"] = joinpath(config["data_dir"], "6h") |> normpath
# daily output dir
config["day_dir"] = joinpath(config["data_dir"], "day") |> normpath
# monthly output dir
config["month_dir"] = joinpath(config["data_dir"], "mon") |> normpath

# expected model output filenames should be: ${filename_prefix}YYYYMM${filename_suffix}
# filename prefix
config["filename_prefix"] = join([config["experiment"], "_"])
# filename suffix
config["filename_suffix"] = ".01.nc"

# cdo config data
config["NAMELIST_afterburner"] = joinpath(config["data_dir"], "NAMELIST_afterburner") |> normpath

config["freq"] = "6h"
config["realization"] = "r01"
```

To change the number of parallel execution instances please change the parameter at the begining of the file:
``` julia
# number of processes to run this script
const PROCESSES = 2
```

# Execute
## mistral
Export the cdi.jl files:
**You must change `[YOUR USER]`!**
``` bash
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/sw/rhel6-x64/eccodes/eccodes-2.6.0-gcc64/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/pf/m/[YOUR USER]/local/lib
```

Execute the TEM-Analyse-Pipeline on mistral
``` bash
cd tem-analyse-pipeline
srun --partition=compute --nodes=1 --account=mh0287 --time=01:00:00 --output=tem_calc.o%j --error=tem_calc.e%j --job-name=tem_calc julia --project=@. tem-calc.jl
```