#! /usr/bin/env julia
#_____________________________________________________________________________
# load packages

using Printf
using Dates
using NCDatasets

#_____________________________________________________________________________
function main()

    # user input/definitions

    # name of experiment (filename prefix)
    base_dir = "/work/bm0550/m214074/echam5-ham"
    experiment = "laach002_l95_t63"
    realization = "r01"
    # frequency_of_input_data
    freq = "6h"
    #freq = "day"
    #freq = "month"
    first_year = 2016
    last_year = 2016

    output_dir = "/scratch/m/m214089/tem"

    # 6 hourly output directory
    work_dir = joinpath(output_dir, "6h")
    # daily output dir
    day_dir = joinpath(output_dir, "day")
    # monthly output dir
    month_dir = joinpath(output_dir, "month")

    # expected model output filenames should be:
    # $(filename_prefix)YYYYMM$(filename_suffix)
    filename_prefix = "$(experiment)_"
    filename_suffix = ".01.nc"

    # TEM diag scripts directory
    temdir = joinpath(base_dir, "tem")
    #_________________________________________________________________________
    # local function definition

    searchdir(path,key) = filter(x->occursin(key,x), readdir(path))

    #_________________________________________________________________________
    # original model output can be found here:
    data_dir = joinpath(base_dir, experiment)

    # output directory will be created, if not already there
    mkpath(work_dir)

    # output directory for daily and monthly data will be created, if not
    # already there
    mkpath(day_dir)
    mkpath(month_dir)

    cd(work_dir)

    println(pwd())

    full_reference_file = joinpath(data_dir, "$(filename_prefix)$(first_year)01$(filename_suffix)")
    if ispath(full_reference_file)

        ds = Dataset(full_reference_file)
        hyam = ds["hyam"][:]
        hybm = ds["hybm"][:]
        close(ds)

        plev = similar(hyam)
        plev .= hyam .+ 101325.0 * hybm
        levels = join(reverse(plev), ",")

        # create afterburner namelist
        open("NAMELIST_afterburner", "w") do output
            write(output,  """
        \$select
          CODE=130,131,132,133,135,149,153,154,
          LEVEL=$(levels),
          TYPE=30,
          FORMAT=2,
        \$end
        """)
        end
    end

    #_________________________________________________________________________
    #

    for year in first_year:last_year

        if !isfile("$(experiment)_$(year)_tem.nc")

            # should be a parallel block

            # transform spectral to grid point
            for month_in_year in 1:12
                month = @sprintf "%02d" month_in_year;
                file = joinpath(data_dir, "$(filename_prefix)$(year)$(month)$(filename_suffix)")
                if ispath(file)
                    atm1 = "$(filename_prefix)$(year)$(month)_atm1.nc"
                    log1 = "$(filename_prefix)$(year)$(month)_atm1.log"
                    println("CDO: after $(file) $(atm1)")
                    run(pipeline(`cdo after $(file) $(atm1)`, stdin="NAMELIST_afterburner")) 
                end
            end

            # synchronization point: continue only if all 12 months are processes

            # should be a parallel block

            # call tem diagnostics
            for month_in_year in 1:12
                month = @sprintf "%02d" month_in_year;
                file = joinpath(data_dir, "$(filename_prefix)$(year)$(month)$(filename_suffix)")
                if ispath(file)
                    log1 = "$(filename_prefix)$(year)$(month)_tem1.log"
                    println("NCL: TEM analysis of expm=\"$(filename_prefix)\" year=\"$(year)\" month=\"$(month)\"")
                    ncl_script = joinpath(temdir, "tem_lon.ncl")
                    run(`ncl expm=\"$(filename_prefix)\" year=\"$(year)\" month=\"$(month)\" $(ncl_script)`)
                end
            end

            # synchronization point: continue only if all 12 months are processed

            # merge files ...

            tfiles = searchdir(work_dir, Regex("$(filename_prefix).*tem1.nc"))

            #  6 hourly data

            # TEM output filename
            ofilename = "tem_$(freq)EPlev_MPI-ESM-LR_$(experiment)_$(realization)_$(year)010100-$(year)123118.nc"
            println("CDO: mergetime")
            run(`cdo mergetime $(tfiles) $(ofilename)`)

            # create daily and monthly mean data

            # TEM output filename
            dfilename = "tem_dayEPlev_MPI-ESM-LR_$(experiment)_$(realization)_$(year)0101-$(year)1231.nc"
            println("CDO: daily mean")
            run(`cdo daymean $(ofilename) $(day_dir)/$(dfilename)`)

            # TEM output filename
            mfilename = "tem_monEPlev_MPI-ESM-LR_$(experiment)_$(realization)_$(year)01-$(year)12.nc"
            println("CDO: monthly mean")
            run(`cdo monmean $(day_dir)/$(dfilename) $(month_dir)/$(mfilename)`)

            # delete temporary data files
            # foreach(rm, filter(endswith(".atm1.nc"), readdir()))
            # foreach(rm, filter(endswith(".tem1.nc"), readdir()))

            println("Processing done for year $(year) ...")
        else
            println("No input data available ...")
            return
        end
    end

    # delete level_extraction log files
    # rm("level_extraction.log")

    # delete cdo after log files
    # foreach(rm, filter(endswith(".atm1.log"), readdir()))

    # delete cdo after namelist
    # rm("SELECT1")

end

#_____________________________________________________________________________
# run program
println("Start processing: ", Dates.format(Dates.now(), "yyyy-mm-dd HH:MM:SS"))

main()

println("Finish processing: ", Dates.format(Dates.now(), "yyyy-mm-dd HH:MM:SS"))
