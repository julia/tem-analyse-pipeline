#----------------------------------------------------------------------------
# Developed by Volker Neff based on a NCL script by Felix Bunzel
# February 2021
#----------------------------------------------------------------------------


using cdi
using cdi.cdiDatasets

include("ncl.jl")
using .ncl

using FitPack

using Base: OneTo
using Dates

# constants
# const PI   = 3.1415926d    # pi is used as π from default julia 
const RD    = 287.05       # gas constant for dry air [J/K/kg]
const CPD   = 1005.46      # specific heat for dry air [J/K/kg]
const PS    = 101325.0     # surface pressure [Pa]
const KAPPA = RD / CPD        # kappa
const A     = 6371000.0    # earth radius [m]
const H     = 7000.0       # scale height [m]
const G     = 9.80665      # gravity acceleration [m/s^2]
const DAY   = 86400.0      # day length [s]
const OMEGA = 2.0 * pi / DAY   # earth angular rotation velocity [1/s]

function streamReadVarWithoutMiss(streamID::Cint, varID::Cint, dims...)
    data = Array{Float64}(undef, dims)
    miss = streamReadVar(streamID, varID, reshape(data, :))
    
    if miss > 0
        @warn "read missing values at var " * string(varID)
    end

    # @info "Read var " * string(varID) * " with dimensions " * debugDims(dims)

    return data
end

function streamWriteVarWithoutMiss(streamID::Cint, varID::Cint, data::AbstractArray{Float64})
    streamWriteVar(streamID, varID, reshape(data, :), 0)
    return nothing
end

"""
The function dfdp calculates the level-derivative of a 2D input
function ff(lev,lat). The given function ftcurvd of the FITGRID
package is used for the derivation process. It calculates the
derivatives of an interpolatory spline under tension.
For the sake of accuracy, coordinate transformation and scaling
is applied.

# Parameter
- `ff`: input 2D array (lev,lat)
- `lev`: associated array of levels
- `alpha`: scaling factor
"""
function dfdp(ff::AbstractArray{Float64}, lev::AbstractVector, alpha::Number)
    # coordinate transformation to z = -ln(p)
    x = .- log.(lev)

    # create arrays
    dydx = similar(ff)
    result = similar(ff)

    for ilat in axes(ff, 1)
        # scaling
        y = ff[ilat, :] ./ (lev.^alpha)

        # derivation
        dydx[ilat, :] = ftcurvd(x, y, x)

        # coordinate transformation back to the p-system
        dydx[ilat, :] = .- dydx[ilat, :] ./ lev

        # inverse scaling
        @. result[ilat, :] = (lev^alpha) * (dydx[ilat, :] + (alpha * y / lev))
    end

    return result
end

"""
The function ifdp calculates the level-integral of an
n-dimensional input function ff(...,lat), where the rightmost
is the latitude. The given function ftcurvi of the FITGRID
package is used for the integration process. It calculates
integrals of an interpolatory spline under tension between
user-specified limits.

This function expects two parameters:

1st: input 1D array (lev), type: double
2nd: associated array of all relevant levels, type: double
"""
function ifdp(ff::Vector{Float64}, lev::Vector{Float64})::Float64
    return ftcurvi(minimum(lev), maximum(lev), lev, ff)
end

"""
The function dfdphi calculates the latitude-derivative of an
n-dimensional input function ff(...,lat), where the rightmost
is the latitude. The given function ftcurvd of the FITGRID
package is used for the derivation process. It calculates the
derivatives of an interpolatory spline under tension.
The input latitude-grid starts at the north pole. This is why
a coordinate transformation from to -lat is applied, and reversed
after the derivation.

This function expects two parameters:

1st: input nD array (...,lat), type: double
2nd: associated array of latitudinal grid points, type: double
"""
function dfdphi(ff::Vector{Float64}, lat::Vector{Float64})
    # coordinate transformation to -lat
    x = similar(lat)
    @. x = -lat
    
    # derivation
    dydx = ftcurvd(x, ff, x)

    @. dydx = -dydx

    return dydx
end

function dfdphi(ff::Array{Float64,2}, lat::Vector{Float64})
    # coordinate transformation to -lat
    x = similar(lat)
    @. x = -lat

    dydx = similar(ff)
    
    # derivation
    for i in axes(ff, 2)
        tmp = ftcurvd(x, ff[:, i], x)
        @. dydx[:, i] = - tmp
    end

    return dydx
end

# help function to test a variable name
hasVar(ds, name) = haskey(ds, name) || @error "$name is not define"

"""
# input
- 'inputFile': input file path
- `outputFile`: output file path
- `varnames`: Dcit that defines the variable names in the input file. It must contain following keys: 'temperature', `v_velocity`, 'u_velocity' and 'vertical_velocity'. If you dose not change the numTrace value to zero it also must contain `specific_humidity
- `numTraces`: Number of traces. Must change to zero if no 'specific_humidity' is defined in the input file
- `vertical_velocity_input`: Set to "omega" for (Pa/s) or "w" for (m/s) depend on the vertical velocity unit. If you set `vertical_velocity_input=="w"` than you need to add `pressure` as key to `varnames`
"""
function tem_lon(inputFile::AbstractString, outputFile::AbstractString, varnames;  numTraces = 1, vertical_velocity_input="omega")::Integer

    @assert haskey(varnames, "temperature")
    @assert haskey(varnames, "v_velocity")
    @assert haskey(varnames, "u_velocity")
    @assert haskey(varnames, "vertical_velocity")
    if numTraces > 0
        @assert haskey(varnames, "specific_humidity")
    end

    @assert vertical_velocity_input == "omega" || vertical_velocity_input == "w"
    if vertical_velocity_input == "w"
        @assert haskey(varnames, "pressure")
    end

    @info "start tem_lon" * " on process " * string(getpid()) * " with Thread " * string(Threads.threadid())
    
    # Load input data
    inputDS = cdiDataset(inputFile)

    # Select variable
    hasVar(inputDS, varnames["temperature"])
    hasVar(inputDS, varnames["u_velocity"])
    hasVar(inputDS, varnames["v_velocity"])
    hasVar(inputDS, varnames["vertical_velocity"])
    if numTraces > 0
        hasVar(inputDS, varnames["specific_humidity"])
    end

    # pressure correction for hPa input levels
    p_corr = 1
    if unit(zAxis(inputDS[varnames["temperature"]])) == "hPa" || unit(zAxis(inputDS[varnames["temperature"]])) == "millibars"
        p_corr = 100
    end

    # Get grid meta
    inputLonSteps = size(inputDS[varnames["temperature"]])[1]
    inputLatSteps = size(inputDS[varnames["temperature"]])[2]
    
    # create output grid and modify 'longitude'
    outputGrid = Gaussian([0.], grid(inputDS[varnames["temperature"]]).yValues)

    # create output time axis
    outputTaxis = duplicateTAxis(tAxis(inputDS))

    # create output z-axis
    outputZaxis = zAxis(inputDS[varnames["temperature"]])
    inputLevels = length(zAxis(inputDS[varnames["temperature"]]))

    # define netCDF output variables
    temVar(name, stdName, unit, code) = Variable(name, outputGrid, outputZaxis, TIME.VARYING; stdName=stdName, longName=stdName, unit=unit, code=code)
    vars = [
        temVar("vs", "residual mean v-velocity", "m/s", 132)
        temVar("ws", "residual mean w-velocity", "m/s", 135)
        temVar("mpsi", "eddy imposed mass-streamfunction", "kg/s", 137)
        temVar("mchi", "residual mass-streamfunction", "kg/s", 136)
        temVar("ub", "zonal mean zonal wind", "m/s", 131)
        temVar("fuy", "Eliassen-Palm flux phi-component", "kg/s^2", 1)
        temVar("fuz", "Eliassen-Palm flux z-component", "kg/s^2", 2)
        temVar("duvsad", "du/dt by vs-advection", "m/s/day", 3)
        temVar("duwsad", "du/dt by vs-advection", "m/s/day", 4)
        temVar("duepy", "du/dt by EP-flux phi-divergence", "m/s/day", 5)
        temVar("duepz", "du/dt by EP-flux z-divergence", "m/s/day", 6)
        temVar("tb", "zonal mean temperature", "k", 130)
        temVar("fthz", "temperature EP-flux z-component", "kg/s", 12)
        temVar("dtvsad", "dT/dt by vs-advection", "K/day", 13)
        temVar("dtwsad", "dT/dt by ws-advection", "K/day", 14)
        temVar("dtepz", "dT/dt by temp. EP-flux z-divergence", "K/day", 16)
    ]

    if numTraces > 0
        traceVars = [
            temVar("xb", "zonal mean H2O", "kg/kg", 133)
            temVar("fxy", "H2O EP-flux phi-component", "kg/s/m^2", 21)
            temVar("fxz", "H2O EP-flux z-component", "kg/s/m^2", 22)
            temVar("dxvsad", "d(H2O)/dt by vs-advection", "ppm/day", 23)
            temVar("dxwsad", "d(H2O)/dt by ws-advection", "ppm/day", 24)
            temVar("dxepy", "d(H2O)/dt by H2O EP-flux phi-divergence", "ppm/day", 1)
            temVar("dxepz", "d(H2O)/dt by H2O EP-flux z-divergence", "ppm/day", 2)
        ]
        append!(vars, traceVars)
    end


    # create output stream
    outputDS = cdiDataset(outputFile, FileTypes.FILETYPE_NC, outputTaxis, vars)

    # constants
    levs = levels(zAxis(inputDS[varnames["temperature"]])) * p_corr
    latrad = deg2rad.(grid(inputDS[varnames["temperature"]]).yValues)
    latrad_cos = cos.(latrad)
    latrad_tan = tan.(latrad)
    f(x) = 2 * OMEGA * sin(x)
    f = f.(latrad)

    # cdi has reverse dimension to ncl
    dim = size(inputDS[varnames["temperature"]])

    # Alloc mem
    st = Array{Float64}(undef, dim...)
    u = Array{Float64}(undef, dim...)
    v = Array{Float64}(undef, dim...)
    omega = Array{Float64}(undef, dim...)
    q = Array{Float64}(undef, dim...)

    td = Array{Float64}(undef, dim...)
    ud = Array{Float64}(undef, dim...)
    vd = Array{Float64}(undef, dim...)
    od = Array{Float64}(undef, dim...)

    if numTraces > 0
        xd = Array{Float64}(undef, numTraces, dim...)
        xb = Array{Float64}(undef, numTraces, inputLatSteps, inputLevels)
        vxb = Array{Float64}(undef, numTraces, inputLatSteps, inputLevels)
        oxb = Array{Float64}(undef, numTraces, inputLatSteps, inputLevels)
        fxy = Array{Float64}(undef, numTraces, inputLatSteps, inputLevels)
        fxz = Array{Float64}(undef, numTraces, inputLatSteps, inputLevels)
        dxvsad = Array{Float64}(undef, numTraces, inputLatSteps, inputLevels)
        dxwsad = Array{Float64}(undef, numTraces, inputLatSteps, inputLevels)
        dxepy = Array{Float64}(undef, numTraces, inputLatSteps, inputLevels)
        dxepz = Array{Float64}(undef, numTraces, inputLatSteps, inputLevels)
    end

    thb = Array{Float64}(undef, inputLatSteps, inputLevels)
    vthb = Array{Float64}(undef, inputLatSteps, inputLevels)
    othb = Array{Float64}(undef, inputLatSteps, inputLevels)
    psi = Array{Float64}(undef, inputLatSteps, inputLevels)
    chi = Array{Float64}(undef, inputLatSteps, inputLevels)
    ws = Array{Float64}(undef, inputLatSteps, inputLevels)
    fuz = Array{Float64}(undef, inputLatSteps, inputLevels)
    fuy = Array{Float64}(undef, inputLatSteps, inputLevels)
    duvsad = Array{Float64}(undef, inputLatSteps, inputLevels)
    duwsad = Array{Float64}(undef, inputLatSteps, inputLevels)
    fthz = Array{Float64}(undef, inputLatSteps, inputLevels)
    dtvsad = Array{Float64}(undef, inputLatSteps, inputLevels)
    duepy = Array{Float64}(undef, inputLatSteps, inputLevels)
    duepz = Array{Float64}(undef, inputLatSteps, inputLevels)
    dtwsad = Array{Float64}(undef, inputLatSteps, inputLevels)
    dtvsad = Array{Float64}(undef, inputLatSteps, inputLevels)
    dtepz = Array{Float64}(undef, inputLatSteps, inputLevels)
    vs = Array{Float64}(undef, inputLatSteps, inputLevels)

    while getNextTimestep(inputDS)
        # set the date and time for this timestep
        setNextTimestep!(outputDS, DateTime(inputDS))

        # load input data
        loadData!(st, inputDS[varnames["temperature"]])
        loadData!(u, inputDS[varnames["u_velocity"]])
        loadData!(v, inputDS[varnames["v_velocity"]])
        loadData!(omega, inputDS[varnames["vertical_velocity"]])
        if numTraces > 0
            loadData!(q, inputDS[varnames["specific_humidity"]])
        end

        # convert w to omega
        if vertical_velocity_input == "w"
            p = loadData(inputDS[varnames["pressure"]])
            # Yet omega contains the (m/s) values
            omega = convert_w2omega(omega, p, st)
            # now omega contains the (Pa/s) values
        end

        @info "Sum st"
        @info sum(st)
        @info "Sum u"
        @info sum(u)
        @info "Sum v"
        @info sum(v)
        @info "Sum omega"
        @info sum(omega)
        
        # calculate zonal means
        dimAvgLon(x) = dimAvgFirst(x)
        tb = dimAvgLon(st)
        ub = dimAvgLon(u)
        vb = dimAvgLon(v)
        ob = dimAvgLon(omega)

        tb_ = dimAvg(st, 1)
        if tb != tb_
            @info "Original: " * string(reshape(tb, :)[1])
            @info "new: " * string(reshape(tb_, :)[1])
            @error "dimAvg Error"
            exit()
        end

        let
            tmp_tb = addDimension(tb, inputLonSteps)
            tmp_ub = addDimension(ub, inputLonSteps)
            tmp_vb = addDimension(vb, inputLonSteps)
            tmp_ob = addDimension(ob, inputLonSteps)
            @. td = st - tmp_tb
            @. ud = u - tmp_ub
            @. vd = v - tmp_vb
            @. od = omega - tmp_ob
        end

        vub = dimAvgLon(vd .* ud)
        oub = dimAvgLon(od .* ud)

        let
            tmp_a = dimAvgLon(vd .* td)
            tmp_b = dimAvgLon(od .* td)
            c = (PS ./ addDimension(levs, inputLatSteps)).^KAPPA            
            @. thb = tb * c
            @. vthb = tmp_a * c
            @. othb = tmp_b * c
        end

        if numTraces > 0
            xb[1, :, :] = dimAvgLon(q)
            xd[1, :, :, :] = q .- addDimension(xb[1, :, :], inputLonSteps)
            vxb[1, :, :] = dimAvgLon(vd .* xd[1, :, :, :])
            oxb[1, :, :] = dimAvgLon(od .* xd[1, :, :, :])

            # ADD OTHER TRACERS HERE !!!
        end

        @info "Processing time step " * string(getTimestep(inputDS)) * " at " * Dates.format(DateTime(inputDS), "yyyy-mm-dd HH:MM") * " on process " * string(getpid()) * " with Thread " * string(Threads.threadid())

        # ; apply TEM analysis
        # ; for dfdp derivatives scaling is applied for better accuracy
        # ; (variables are rescaled after the derivation process).
        # ; the scale factor is the 3rd parameter of the dfdp-call.
    
        # ; ATTENTION: For the sake of simplicity, the p-system is used for calculation of
        # ;            all TEM quantities. Afterwards, variables are transformed back to
        # ;            the z-system, so that the output is in the z-system.
    
        # eddy imposed velocity-streamfunction
        let
            tmp = dfdp(thb, levs, -KAPPA)
            @. psi = -vthb[:,:] / tmp
            @info "Sum psi"
            @info sum(psi)
            @info "Sum vthb"
            @info sum(vthb)
            @info "Sum dfdp"
            @info sum(tmp)
        end
        
        # residual velocity-streamfunction
        for ilat in OneTo(inputLatSteps)
            # for the uppermost level the integral ifdp (see below) becomes zero
            chi[ilat,end] = psi[ilat,end]

            # for all other levels the integral ifdp is used
            for ilev in OneTo(inputLevels - 1)
                tmp_vb = reverse(vb[ilat, :])
                tmp_levels = reverse(levs)
                chi[ilat,ilev] = ifdp(tmp_vb[begin:end - ilev + 1], tmp_levels[begin:end - ilev + 1]) + psi[ilat, ilev]
            end
        end

        # v-component of the residual mean meridional circulation
        let 
            tmp = dfdp(psi, levs, 1)
            @. vs = vb + tmp
        end
        

        # level-by-level calculation of further variables
        # (this is due to the different dimensionality of the arrays involved in these equations)
        for ilev in OneTo(inputLevels)
            # w-component of the residual mean meridional circulation
            tmp_a = dfdphi(latrad_cos .* psi[:, ilev], latrad)
            @. ws[:, ilev] = ob[:, ilev] - (tmp_a / A / latrad_cos)

            # another way to calculate ws is the following (make your choice!):
            # ws(ilev,:) = (-dfdphi(chi(ilev,:),latrad) + (tan(latrad) * chi(ilev,:))) / A
            # for this solution, the following is assumed:
            # ob = -dfdphi(cos(phi)*ifdp(vb(TOP:ilev))) / A / cos(phi)

            # z-component of the Eliassen-Palm flux
            # z-component of the Eliassen-Palm flux
            tmp_b = dfdphi(ub[:, ilev], latrad)
            @. fuz[:, ilev] = (-psi[:, ilev] * ((-(tmp_b - (ub[:, ilev] * latrad_tan)) / A) + f) - oub[:, ilev]) * A * latrad_cos

            # vs-contribution to du/dt
            @. duvsad[:, ilev] = -vs[:, ilev] * (((tmp_b - (latrad_tan * ub[:, ilev])) / A) - f) * DAY
        
            # tracers
            for i in OneTo(numTraces)
                tmp_c = dfdphi(xb[i, :, ilev], latrad)
                @. fxz[i, :, ilev] = (tmp_c / A * psi[:, ilev]) - oxb[i, :, ilev]
                @. dxvsad[i, :, ilev] = -vs[:, ilev] * tmp_c / A * DAY * 1e6
            end
            
        end

        # ws-contribution to du/dt
        let 
            tmp = dfdp(ub, levs, 0)
            @. duwsad = -ws * tmp * DAY
        end
        

        # z-component of the temperature-EP flux, y-component is zero (always?)
        let 
            tmp = dfdphi(thb, latrad)
            @. fthz = (tmp / A * psi) - othb
        end
        

        # vs-contribution to dT/dt
        let 
            tmp = dfdphi(tb, latrad)
            @. dtvsad = -vs * tmp / A * DAY
        end
        
        # 2D precalculation of further variables (see next lines)
        fuy_temp    = (.-dfdp(ub, levs, 0) .* psi) .- vub
        duepz_temp  = dfdp(fuz, levs, 1) ./ A
        dtwsad_temp = .-ws .* dfdp(thb, levs, -KAPPA)
        dtepz_temp  = dfdp(fthz, levs, KAPPA)

        let 
            tmp = cos.(addDimension2(latrad, inputLevels))
            @. fuy = fuy_temp * A * tmp
        end

        for i in OneTo(numTraces)
            tmp_a = dfdp(xb[i,:,:], levs, 0)
            @. fxy[i, :,:] = (-tmp_a * psi) - vxb[i,:,:]
            @. dxwsad[i,:,:] = -ws * tmp_a * DAY * 1e6
            dxepz[i,:,:]  = dfdp(fxz[i,:,:], levs, 1) .* DAY .* 1e6
        end

        # level-by-level calculation of further variables
        # (this is due to the different dimensionality of the arrays involved in these equations)
        for ilev in OneTo(inputLevels)
            # div(F)-y-contribution to du/dt
            tmp_a = dfdphi(fuy[:, ilev], latrad)
            @. duepy[:, ilev] = ((tmp_a - (latrad_tan * fuy[:, ilev])) / A) / (A * latrad_cos) * DAY

            # div(F)-z-contribution to du/dt
            @. duepz[:, ilev] = duepz_temp[:, ilev] / latrad_cos * DAY

            # ws-contribution to dT/dt
            @. dtwsad[:, ilev] = dtwsad_temp[:, ilev] * ((levs[ilev] / PS) ^ KAPPA) * DAY

            # fthz-contribution to dT/dt
            @. dtepz[:, ilev] = dtepz_temp[:, ilev] * ((levs[ilev] / PS) ^ KAPPA) * DAY

            # conversion back to z-coordinates
            @. ws[:, ilev]   = -(H / levs[ilev]) * ws[:, ilev]
            @. fuy[:, ilev]  = fuy[:, ilev] * levs[ilev] / (H * G)
            @. fuz[:, ilev]  = -fuz[:, ilev] / G
            @. fthz[:, ilev] = -CPD / G * fthz[:, ilev]

            # convert velocity stream functions to mass stream functions
            tmp_b = 2 * pi * A * latrad_cos / G 
            @. psi[:, ilev]  = tmp_b * psi[:, ilev]
            @. chi[:, ilev]  = tmp_b * chi[:, ilev]

            # tracers
            for i in OneTo(numTraces)
                tmp = dfdphi(fxy[i,:,ilev], latrad)
                @. dxepy[i,:,ilev] = ((tmp - (latrad_tan * fxy[i,:,ilev])) / A) * DAY * 1e6
                @. fxy[i,:,ilev]   = levs[ilev] / H / G * fxy[i,:,ilev]
                @. fxz[i,:,ilev]   = -fxz[i,:,ilev] / G
            end
        end

        # tracers
        for i in OneTo(numTraces)
            @. xb[i, :,:] = xb[i, :,:] * 1e6
        end

        # write variables to netCDF file
        storeData!(outputDS["vs"], vs)
        storeData!(outputDS["ws"], ws)
        storeData!(outputDS["mpsi"], psi)
        storeData!(outputDS["mchi"], chi)
        storeData!(outputDS["ub"], ub)
        storeData!(outputDS["fuy"], fuy)
        storeData!(outputDS["fuz"], fuz)
        storeData!(outputDS["duvsad"], duvsad)
        storeData!(outputDS["duwsad"], duwsad)
        storeData!(outputDS["duepy"], duepy)
        storeData!(outputDS["duepz"], duepz)
        storeData!(outputDS["tb"], tb)
        storeData!(outputDS["fthz"], fthz)
        storeData!(outputDS["dtvsad"], dtvsad)
        storeData!(outputDS["dtwsad"], dtwsad)
        storeData!(outputDS["dtepz"], dtepz)
        if numTraces > 0
            storeData!(outputDS["xb"], xb[1,:,:])
            storeData!(outputDS["fxy"], fxy[1,:,:])
            storeData!(outputDS["fxz"], fxz[1,:,:])
            storeData!(outputDS["dxvsad"], dxvsad[1,:,:])
            storeData!(outputDS["dxwsad"], dxwsad[1,:,:])
            storeData!(outputDS["dxepy"], dxepy[1,:,:])
            storeData!(outputDS["dxepz"], dxepz[1,:,:])
        end

    end

    return 0
end
