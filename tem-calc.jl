#----------------------------------------------------------------------------
# Developed by Volker Neff
# February 2021
#----------------------------------------------------------------------------


# load local modules
using Dates
using Distributed # will automaticly load on all working processes

# number of processes to run this script
const PROCESSES = 2

# create new process tasks
#procs = addprocs(PROCESSES, exeflags="--project=@.", topology=:all_to_all)
@info procs # get all internal process ids like myid()

# init the project envioment on all processes
@everywhere function init()
    println("Init project folder ", @__DIR__, " at process ", myid())
    Pkg.activate(@__DIR__)
    Pkg.instantiate()
end

@everywhere import Pkg
@everywhere init()

# create a internal gard to execute only a few cdo task in parallel
const cdos = RemoteChannel(()->Channel{Bool}(nworkers()))
@assert nworkers() >= 1 "need at least one worker"
for i in 1:nworkers()
    put!(cdos, true)
end

# load modules on all working processes
@everywhere using NCDatasets
@everywhere using Dispatcher

# load TEM-Analyse on all working processes
@everywhere include("tem-lon.jl")

####################### export global RO Vars #######################
# declare config type on all wokres 
@everywhere Config = Dict{String,Any}

const config = Config()
config["experiment"] = "winter1983"

config["firstDate"] = Date(1983, 10)
config["lastDate"]  = Date(1983, 10)

config["base_dir"] = "/work/bm0951/UAICON360/new/" |> normpath

# original model output can be found here:
config["data_dir"] = joinpath(config["base_dir"], config["experiment"]) |> normpath

# 6 hourly output directory
config["work_dir"] = joinpath("/scratch/m/m300859/", "6h") |> normpath
# daily output dir
config["day_dir"] = joinpath("/scratch/m/m300859/", "day") |> normpath
# monthly output dir
config["month_dir"] = joinpath("/scratch/m/m300859/", "mon") |> normpath

# expected model output filenames should be: ${filename_prefix}YYYYMM${filename_suffix}
# filename prefix
config["filename_prefix"] = join([config["experiment"], "_360l_"])
# filename suffix
config["filename_suffix"] = "T000000Z_N96_p.nc"

# cdo config data
config["NAMELIST_afterburner"] = joinpath(config["data_dir"], "NAMELIST_afterburner") |> normpath

config["freq"] = "6h"
config["realization"] = "r01"

config["var_names"] = Dict(
    "temperature" => "ta",
    "v_velocity" => "va",
    "u_velocity" => "ua",
    "vertical_velocity" => "wa"
)

####################### export global functions ####################### 

@everywhere filename(prefix, date, suffix) = join([prefix, Dates.format(date, "yyyymm"), suffix])

@everywhere function cdo_after(date::Date, config)::String
    file = filename(config["filename_prefix"], date, config["filename_suffix"])
    full_path = joinpath(config["data_dir"], file)
    if ispath(full_path)
        atm1 = joinpath(config["work_dir"], filename(config["filename_prefix"], date, "_atm1.nc"))

        println("CDO: after $(full_path) $(atm1)")
        run(pipeline(`cdo after $(full_path) $(atm1)`, stdin=config["NAMELIST_afterburner"]))
        return atm1
    else
        error("can't find " * full_path)
    end
end

@everywhere function tem_analyse(date::Date, atm1, config::Config)
    if ispath(atm1)
        tem1 = joinpath(config["work_dir"], filename(config["filename_prefix"], date, "_tem1.nc"))
        
        println("TEM-Analyze: $atm1 $tem1")
        tem_lon(atm1, tem1, config["var_names"], numTraces = 0)
        return tem1
    else
        error("can't find " * atm1)
    end
end

@everywhere function cdo_mergetime(date::Date, config::Config, tems...)
    # TEM output filename
    output_file = join([
        "tem_", config["freq"], "EPlev_MPI-ESM-LR_", config["experiment"], "_", 
        config["realization"], "_", string(year(date)), "010100-", 
        string(year(date)), "123118.nc"
        ])
    output_path = joinpath(config["work_dir"], output_file)
    input_files = join(tems, " ")

    println("CDO: mergetime: ", input_files, "   ", output_file)
    run(`cdo mergetime $(input_files) $(output_path)`)
    return output_path
end

@everywhere function cdo_daymean(date::Date, input::AbstractString, config::Config)
    output_file = join([
        "tem_dayEPlev_MPI-ESM-LR_", config["experiment"], "_", 
        config["realization"], "_", string(year(date)), "0101-", 
        string(year(date)), "1231.nc"
        ])
    output_path = joinpath(config["day_dir"], output_file)
    println("CDO: daily mean")
    run(`cdo daymean $(input) $(output_path)`)
    return output_path
end

@everywhere function cdo_monthmean(date::Date, input::AbstractString, config::Config)
    output_file = join([
        "tem_dayEPlev_MPI-ESM-LR_", config["experiment"], "_", 
        config["realization"], "_", string(year(date)), "01-", 
        string(year(date)), "12.nc"
        ])
    output_path = joinpath(config["month_dir"], output_file)
    println("CDO: monthly mean")
    run(`cdo monmean $(input) $(output_path)`)
    return output_path
end

# Function that takes dependencies but do not pass them to the underlaing function
#@everywhere funDep(f::Function, args, dep...) = f(args...)

# wrap the cdo calls to be shure that only a few calls can be execute in parallel.
# This can save bandwith and RAM
@everywhere function cdo_call(cdos, f::Function, args...)
    ret = nothing
    try
        take!(cdos)
        ret = f(args...)
    catch e
        println(e)
        throw(e)
    finally
        put!(cdos, true)
    end
    return ret
end

####################### Main ####################### 

# start time of processing
@info "Start processing: " * Dates.format(now(), "yyyy-mm-dd HH:MM:SS")
@info "Use " * string(Threads.nthreads()) * " threads for processing"
@info "Use " * string(nprocs()) * " processes for processing"
@info "Use " * string(nworkers()) * " workers for processing"

# create 
file = filename(config["filename_prefix"], config["firstDate"], config["filename_suffix"])
full_reference_file = joinpath(config["data_dir"], file) |> normpath
#if ispath(full_reference_file)
#
#    ds = Dataset(full_reference_file)
#    hyam = ds["hyam"][:]
#    hybm = ds["hybm"][:]
#    close(ds)
#
#    plev = similar(hyam)
#    plev .= hyam .+ 101325.0 * hybm
#    local levels = join(reverse(plev), ",")
#
#    # create afterburner namelist
#    open(config["NAMELIST_afterburner"], "w") do output
#        write(output,  """
#            \$select
#                CODE=130,131,132,133,135,149,153,154,
#                LEVEL=$(levels),
#                TYPE=30,
#                FORMAT=2,
#            \$end
#            """)
#    end
#else
#    @error "can't find " * full_reference_file
#    exit(-1)
#end

# output directory will be created, if not already there
mkpath(config["work_dir"])

# output directory for daily and monthly data will be created, if not
# already there
mkpath(config["day_dir"])
mkpath(config["month_dir"])

cd(config["work_dir"])

@info "Working directory: " * pwd()

cdo_mms = Vector{DispatchNode}(undef, 0)
for y in year(config["firstDate"]):year(1):year(config["lastDate"])
    
    # execute only if file not exist
    if !isfile(join([config["experiment"], "_", string(year(y)), "_tem.nc"]))
        tems = Vector{DispatchNode}(undef, 0)
        begin_date = max(Date(y), config["firstDate"])
        end_date = min(Date(y,12), config["lastDate"])
        for d in begin_date:Month(1):end_date

            # call cdo after to extract required variables from model data
            #atm = @op cdo_call(cdos, cdo_after, d, config)
            file = filename(config["filename_prefix"], d, config["filename_suffix"])
            atm = joinpath(config["data_dir"], file)

            # call tem diagnostics
            tem = @op tem_analyse(d, atm, config)
            push!(tems, tem)
        end

        # merge files ...
        cdo_mt = @op cdo_call(cdos, cdo_mergetime, Date(y), config, tems...)
        

        ### create daily and monthly mean data ###
        # TEM output filename
        cdo_dm = @op cdo_call(cdos, cdo_daymean, Date(y), cdo_mt, config)

        # TEM output filename
        cdo_mm = @op cdo_call(cdos, cdo_monthmean, Date(y), cdo_dm, config)
        push!(cdo_mms, cdo_mm)
    end
end

# Execute the full dependency graph
run!(ParallelExecutor(), cdo_mms)
