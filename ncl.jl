#----------------------------------------------------------------------------
# Developed by Volker Neff
# February 2021
#----------------------------------------------------------------------------


module ncl

using Statistics

"""
gnerate the average of the selected dimension in the input array
"""
function dimAvg(data::AbstractArray, dim::Integer)
    inputDim = size(data)
    @assert length(inputDim) > 1 "data has not enough dimensions"
    pre = inputDim[begin:dim - 1]
    post = inputDim[dim + 1:end]
    outputDim = [pre..., post...]

    output = Array{eltype(data)}(undef, outputDim...)
    for i in CartesianIndices(post)
        for j in CartesianIndices(pre)
            output[i, j] = mean(data[j,:, i])
        end
    end

    return output
end

function dimAvgFirst(data::AbstractArray)
    inputDim = size(data)
    @assert length(inputDim) > 1 "data has not enough dimensions"
    post = inputDim[2:end]
    outputDim = [post...]

    output = Array{eltype(data)}(undef, outputDim...)
    for i in CartesianIndices(post)
        d = data[:, i]    
        output[i] = mean(d)
    end

    return output
end

function dimAvgFirst(data::Array{T, 3}) where {T}
    inputDim = size(data)
    post = inputDim[2:end]
    outputDim = [post...]

    output = Array{eltype(data)}(undef, outputDim...)
    for x in axes(data, 2)
        for y in axes(data, 3) 
            d = data[:, x, y]
            output[x,y] = mean(d)
        end
    end

    return output
end

#dimAvgFirst(data) = dimAvg(data, 1)
dimAvgLast(data) = dimAvg(data, length(size(data)))

export dimAvg, dimAvgFirst, dimAvgLast

# TODO rewrite this function with Iterators to save memory
# TODO see broadcast function
"""
Add a dimension to array. All new values will be written like
```
O[1,1] = I[1]
O[2,1] = I[1]
O[n,1] = I[1]
# ...
O[1,2] = I[2]
O[2,2] = I[2]
O[n,2] = I[2]
# ...
O[1,m] = I[m]
O[2,m] = I[m]
O[n,m] = I[m]
```
addDimension is like ncl conform_dims
"""
function addDimension(data::AbstractArray, dimSize::Integer)
    inputDim = size(data)
    outputDim = (dimSize, inputDim...)
    inputType =  eltype(data)

    output = Array{inputType}(undef, outputDim)
    for i in CartesianIndices(inputDim)
        for j in 1:dimSize
            output[j, i] = data[i]
        end
    end

    return output
end

function addDimension2(data::AbstractArray, dimSize::Integer)
    inputDim = size(data)
    outputDim = (inputDim..., dimSize)
    inputType =  eltype(data)

    output = Array{inputType}(undef, outputDim)
    for i in CartesianIndices(inputDim)
        for j in 1:dimSize
            output[i, j] = data[i]
        end
    end

    return output
end
export addDimension, addDimension2

const rgas  = 287.058            # J/(kg-K) => m2/(s2 K)
const g     = 9.80665            # m/s2

"""
# Input
- w: Vertical velocity with units (m/s)
- p: A variable containing pressure values (Pa)
- t: A variable containing temperature (K)
"""
function convert_w2omega(w::Array{T}, p::Array{T}, t::Array{T}) where {T}
    
    rho = similar(p)
    omega = similar(p)

    @. rho   = p/(rgas*t)
    @. omega = -w*rho*g

    return omega
end
export convert_w2omega


end # module ncl
