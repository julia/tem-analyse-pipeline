#!/bin/ksh
#
#SBATCH --job-name=tem_dw        # Specify job name
#SBATCH --partition=compute      # Specify partition name
#SBATCH --nodes=1                # Specify max. number of tasks to be invoked
#SBATCH --time=02:00:00          # Set a limit on the total run time
#SBATCH --account=bm0550         # Charge resources on this project account
#SBATCH --output=my_job.o%j      # File name for standard output
#SBATCH --error=my_job.e%j       # File name for standard error output


set -eu

################### CHANGE HERE ###################
exp=qbo008_l95_t63                                 # name of experiment (filename prefix)
realiz=r01                                 # realization
#freq=day                                  # frequency of input data
freq=6h                                    # frequency of input data
first_year=2013                            # first year of time range
last_year=2013                             # last year of time range

datdir=/work/bm0550/m214074/echam5-ham/${exp}/mon 

# model data directory
workdir=/work/bm0550/m214074/echam5-ham/${exp}/6h  # output directory
daydir=/work/bm0550/m214074/echam5-ham/${exp}/day  # daily output dir
mondir=/work/bm0550/m214074/echam5-ham/${exp}/mon  # monthly output dir

# model output filenames should be: ${filename_prefix}YYYYMM${filename_suffix}
filename_prefix=${exp} # filename prefix
filename_suffix=.01.nc                                 # filename suffix
temdir=/work/bm0550/m214074/echam5-ham/tem  # TEM diag directory
###################################################

# call tem diagnostics

YY=${first_year}
while [[ $YY -le $last_year ]]
do
    if [ -e ${datdir}/${filename_prefix}${YY}${filename_suffix} ]
    then
        ncl 'expm="'${filename_prefix}'"' 'year="'${YY}'"' ./tem_dw_ctrl.ncl 
        mv tem_dwc_ctrl.nc /work/bm0550/m214074/echam5-ham/${exp}/6h/${filename_prefix}${YY}_tem_dw_ctrl${filename_suffix}
    fi
done
