#!/bin/bash
#
#SBATCH --job-name=tem_calc        # Specify job name
#SBATCH --partition=compute        # Specify partition name
#SBATCH --nodes=1                  # Specify max. number of tasks to be invoked
#SBATCH --time=02:00:00            # Set a limit on the total run time
#SBATCH --account=bm0550           # Charge resources on this project account
#SBATCH --output=tem_calc.o%j      # File name for standard output
#SBATCH --error=tem_calc.e%j       # File name for standard error output

set -eu

################### CHANGE HERE ###################
exp=qbo010_l95_t63                           # name of experiment (filename prefix)
realiz=r01                                   # realization
#freq=day                                    # frequency of input data
freq=6h                                      # frequency of input data
first_year=2017                              # first year of time range
last_year=2021                               # last year of time range
# original model output can be found here:
datdir=/work/bm0550/m214074/echam5-ham/${exp} 

# 6 hourly output directory
workdir=/work/bm0550/m214074/echam5-ham/${exp}/6h  
# daily output dir
daydir=/work/bm0550/m214074/echam5-ham/${exp}/day  
# monthly output dir
mondir=/work/bm0550/m214074/echam5-ham/${exp}/mon  

# expected model output filenames should be: ${filename_prefix}YYYYMM${filename_suffix}
# filename prefix
filename_prefix=${exp}_                      
# filename suffix
filename_suffix=.01.nc                       
# TEM diag scripts directory
temdir=/work/bm0550/m214074/echam5-ham/tem   
###################################################

# start time of processing
echo "Start processing: " $(date +"%Y-%m-%d %H-%M-%S")

# output directory will be created, if not already there
if [ ! -d $workdir ]; then
    mkdir -p $workdir
fi
# output directory for daily and monthly data will be created, if not already there
if [ ! -d $daydir ]; then
    mkdir -p $daydir
fi
if [ ! -d $mondir ]; then
    mkdir -p $mondir
fi

cd $workdir

found_file=0
levfile=lev.nc
YY=${first_year}
while [[ $YY -le $last_year ]]
do
    for MM in 01 02 03 04 05 06 07 08 09 10 11 12
    do
        if [[ ${found_file} -eq 0 ]]
        then
            if [ -e ${datdir}/${filename_prefix}${YY}${MM}${filename_suffix} ]
            then
                
                # copy model output file to netcdf format
                cdo -f nc seltimestep,1 ${datdir}/${filename_prefix}${YY}${MM}${filename_suffix} ${levfile}
                
                # write ncl file for level extraction
                cat > temlev.ncl <<EOF
          load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
          begin
            x=addfile("${levfile}","r")
            outlev=""
            no_levs=dimsizes(x->hyam)
            do ilev=1,no_levs
              outlev=outlev+((101325.d*x->hybm(no_levs-ilev))+x->hyam(no_levs-ilev))+", "
            end do
            asciiwrite("testlevels.out",outlev)
          end
EOF

                # execute ncl script for level extraction
                ncl temlev.ncl > level_extraction.log 2>&1

                # read levels
                read levs < testlevels.out

                # delete temporary files
                rm temlev.ncl testlevels.out ${levfile}

                # create afterburner namelist

                cat > SELECT1 << EOF
          \$select
            CODE=130,131,132,133,135,149,153,154,
            LEVEL=${levs}
            TYPE=30,
            FORMAT=2,
          \$end
EOF
                # skip execution of commands next time
                found_file=1
            fi
        fi
    done
    YY=$(( YY + 1 ))
done

#_____________________________________________________________________________
#

YY=${first_year}
while [[ $YY -le $last_year ]]
do
    if [ ! -e ${exp}_${YY}_tem.nc ]
    then
        #
        # do parallel processing in subsequent subshells

        (
            # call cdo after to extract required variables from model data
            for MM in 01 02 03 04 05 06 07 08 09 10 11 12
            do
                if [ -e ${datdir}/${filename_prefix}${YY}${MM}${filename_suffix} ]
                then
                    file=${datdir}/${filename_prefix}${YY}${MM}${filename_suffix}
                    atm1=${filename_prefix}${YY}${MM}_atm1.nc
                    log1=${filename_prefix}${YY}${MM}_atm1.log
                    cdo after < SELECT1 $file $atm1 > $log1 2>&1 &
                fi
            done
            wait
        )
        # synchronization point: continue only if all 12 months are processes

        (
            # call tem diagnostics
            for MM in 01 02 03 04 05 06 07 08 09 10 11 12
            do
                if [ -e ${datdir}/${filename_prefix}${YY}${MM}${filename_suffix} ]
                then
                    log1=${filename_prefix}${YY}${MM}_tem1.log                    
                    ncl 'expm="'${filename_prefix}'"' 'year="'${YY}'"' 'month="'${MM}'"' $temdir/tem_lon.ncl  > $log1 2>&1 &
                fi
            done
            wait
        )
        # synchronization point: continue only if all 12 months are processes

        # merge files
        
        tfiles=$(ls ${filename_prefix}*_tem1.nc)
        # TEM output filename
        ofilename=tem_${freq}EPlev_MPI-ESM-LR_${exp}_${realiz}_${YY}010100-${YY}123118.nc 
        cdo mergetime ${tfiles} ${ofilename}

        # create daily and monthly mean data

        # TEM output filename
        dfilename=tem_dayEPlev_MPI-ESM-LR_${exp}_${realiz}_${YY}0101-${YY}1231.nc 
        # TEM output filename
        mfilename=tem_monEPlev_MPI-ESM-LR_${exp}_${realiz}_${YY}01-${YY}12.nc
        cdo daymean ${ofilename} ${daydir}/${dfilename}
        cdo monmean ${daydir}/${dfilename} ${mondir}/${mfilename}

        # delete temporary data files
   rm *atm1.nc
   rm *tem1.nc

        echo Processing done for year $YY...
    fi
    YY=$(( YY + 1 ))
done

# delete level_extraction log files
 rm level_extraction.log

# delete cdo after log files
 rm *atm1.log

# delete cdo after namelist
 rm SELECT1

# end time of processing
echo "Finish processing: " $(date +"%Y-%m-%d %H-%M-%S")

exit

